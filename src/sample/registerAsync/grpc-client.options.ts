import { GrpcOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import { addReflectionToGrpcConfig } from 'src/grpc-reflection/utils';

@Injectable()
export class GrpcClientOptions {
  constructor(private readonly configService: ConfigService) {}

  get getGRPCConfig(): GrpcOptions {
    const grpcPort =
      this.configService.get<number>('WEB3_SERVICE_GRPC_PORT') || 5000;

    return addReflectionToGrpcConfig({
      transport: Transport.GRPC,
      options: {
        url: `0.0.0.0:${grpcPort}`,
        package: 'sample',
        protoPath: join(__dirname, './proto/sample.proto'),
        loader: {
          oneofs: true,
          includeDirs: [join(__dirname, './proto/vendor/')],
        },
      },
    });
  }
}
