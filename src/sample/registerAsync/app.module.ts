import { Module } from '@nestjs/common';
import { GrpcReflectionModule } from '../../grpc-reflection';
import { GrpcClientOptions } from './grpc-client.options';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    GrpcReflectionModule.registerAsync({
      useFactory: async (configService: ConfigService) => {
        const grpcClientOptions: GrpcClientOptions = new GrpcClientOptions(
          configService,
        );
        return grpcClientOptions.getGRPCConfig;
      },
      inject: [ConfigService],
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
