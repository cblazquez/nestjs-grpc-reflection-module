import { DynamicModule, Module, Provider } from '@nestjs/common';
import { GrpcOptions } from '@nestjs/microservices';
import { GrpcReflectionController } from './grpc-reflection.controller';
import { GRPC_CONFIG_PROVIDER_TOKEN } from './grpc-reflection.constants';
import { GrpcReflectionService } from './grpc-reflection.service';
import {
  GrpcReflectionModuleAsyncOptions,
  GrpcReflectionOptionsFactory,
} from './interfaces/grpc-reflection-module-options.interface';

@Module({})
export class GrpcReflectionModule {
  static register(grpcOptions: GrpcOptions): DynamicModule {
    return {
      module: GrpcReflectionModule,
      controllers: [GrpcReflectionController],
      providers: [
        GrpcReflectionService,
        {
          provide: GRPC_CONFIG_PROVIDER_TOKEN,
          useValue: grpcOptions,
        },
      ],
    };
  }

  static registerAsync(
    options: GrpcReflectionModuleAsyncOptions,
  ): DynamicModule {
    return {
      module: GrpcReflectionModule,
      controllers: [GrpcReflectionController],
      imports: options.imports || [],
      providers: this.createAsyncProviders(options),
    };
  }

  private static createAsyncProviders(
    options: GrpcReflectionModuleAsyncOptions,
  ): Provider[] {
    const providers: Provider[] = [GrpcReflectionService];

    if (options.useFactory) {
      providers.push({
        provide: GRPC_CONFIG_PROVIDER_TOKEN,
        useFactory: options.useFactory,
        inject: options.inject || [],
      });
    } else if (options.useExisting) {
      providers.push({
        provide: GRPC_CONFIG_PROVIDER_TOKEN,
        useFactory: async (optionsFactory: GrpcReflectionOptionsFactory) =>
          await optionsFactory.createGrpcReflectionOptions(),
        inject: [options.useExisting],
      });
    } else {
      providers.push({
        provide: GRPC_CONFIG_PROVIDER_TOKEN,
        useFactory: async (optionsFactory: GrpcReflectionOptionsFactory) =>
          await optionsFactory.createGrpcReflectionOptions(),
        inject: [options.useClass],
      });
      providers.push({
        provide: options.useClass,
        useClass: options.useClass,
      });
    }

    return providers;
  }
}
